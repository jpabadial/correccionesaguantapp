<?php

class Distancia{


  function getDistance($lat1, $long1, $lat2, $long2)
  {
  //$earth = 6371; //km change accordingly
  $earth = 6371; //miles

  //Point 1 cords
  $lat1 = deg2rad($lat1);
  $long1= deg2rad($long1);

  //Point 2 cords
  $lat2 = deg2rad($lat2);
  $long2= deg2rad($long2);

  //Haversine Formula
  $dlong=$long2-$long1;
  $dlat=$lat2-$lat1;

  $sinlat=sin($dlat/2);
  $sinlong=sin($dlong/2);

  $a=($sinlat*$sinlat)+cos($lat1)*cos($lat2)*($sinlong*$sinlong);

  $c=2*asin(min(1,sqrt($a)));

  $d=round($earth*$c);

  return $d;
  }

  // pull cords out of database

}

  // echo "Distance in miles from CB2 to SS4: ".getDistance(52.163, 0.133, 51.594, 0.715);
?>
